<?php

namespace App\Listeners;

use BSSystem\MODWebAuth\Events\UserRegistered;
use BSSystem\MODWebAuth\Events\UserUpdated;
use Facades\BSSystem\LIBMember\Repositories\MemberRepo;
use Facades\BSSystem\LIBReseller\Repositories\ResellerRepo;

class UserEventListener
{
    /**
     * Event saat registrasi baru di Account Center ke aplikasi client bersangkutan
     *
     * @param UserUpdated $event
     *      $event->userData = array berisi record table users
     */
    public function onUserRegistered(UserRegistered $event)
    {
        // ....
    }

    /**
     * Event saat data user di Account Center berubah (updated)
     *
     * @param UserUpdated $event
     *      $event->userData = array berisi record table users
     */
    public function onUserUpdated(UserUpdated $event)
    {
        //...
    }

    public function subscribe($events)
    {
        $events->listen(
            UserRegistered::class, 'App\Listeners\UserEventListener@onUserRegistered'
        );

        $events->listen(
            UserUpdated::class, 'App\Listeners\UserEventListener@onUserUpdated'
        );
    }

}
