<?php

namespace App\Modules\Home\Controllers;

use Illuminate\Http\Request;
use App\Modules\Home\Responses\HomeResponse;

use BSSystem\Core\Base\BaseController;

class HomeController extends BaseController
{
    private $repo;
    
    public function __construct()
    {
        
        \Breadcrumb::add(__('user.index'),url(''));        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['data'] = [];
        return new HomeResponse($data,'home.list');
    }
    
}
