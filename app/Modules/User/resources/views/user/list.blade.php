@extends('layouts.app')

@section('content')
<section class="content-header">
  <h1>
    LIST
  </h1>
  {{Breadcrumb::generate()}}
</section>
    
<section class="content">
    <div class="row">

        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">List</h3>
                    <div class="group-btn pull-right">
                        <a href="{{ route('user.create') }}" class="btn btn-success btn-sm">Tambah Tipe Aset</a>
                    </div>                    
                </div>
                <!-- /.box-header -->
                <div class="box-body">                    

                    <table id="data-tables" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td>Name</td><td>Email</td><td>Email Verified At</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $item->name }}</td><td>{{ $item->email }}</td><td>{{ $item->email_verified_at }}</td><td>{{ $item->password }}</td><td>{{ $item->remember_token }}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('user.edit',$item->id) }}" type="button" class="btn btn-info" title="Edit"><i class="fa fa-edit"></i></a>
                                        <form action="{{ route('user.destroy',$item->id) }}" class="delete-form btn btn-danger delete-form{{$loop->iteration}}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <a data-toggle="delete" class="delete-button" data-id="{{ $loop->iteration }}" id="delete{{ $loop->iteration }}">
                                                <i class="fa fa-remove"></i>
                                            </a>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->  
        </div>
        
    </div>
</section>
<script>
    $(document).ready(function () {
        $('#data-tables').DataTable();
        $('.delete-button').on('click', function () {
            if (confirm('Delete this data ?')) {
                $('.delete-form' + $(this).data('id')).submit();
            }            
        });
    });
</script>
@endsection
