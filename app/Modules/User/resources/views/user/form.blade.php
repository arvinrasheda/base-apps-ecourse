@extends('layouts.app')

@section('content')
<div class="container">
    <section class="content-header">
        <h1>
            FORM
        </h1>
        {{Breadcrumb::generate()}}
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ $mode=='add'?'Tambah Baru':'Edit Data' }}</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="{{ $mode=='create'?route('user.store'):route('user.update',$data->id) }}">
                        {{ csrf_field() }}
                        {{ $mode=='edit'?method_field('PATCH'):'' }}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="input-email_verified_at">Name</label>
                                <input type="text" name="email_verified_at" class="form-control" id="input-email_verified_at" value="{{ $data ? $data->email_verified_at : old('email_verified_at') }}">
                            </div>
                            <div class="form-group">
                                <label for="input-email_verified_at">Email</label>
                                <input type="text" name="email_verified_at" class="form-control" id="input-email_verified_at" value="{{ $data ? $data->email_verified_at : old('email_verified_at') }}">
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

</div>
@endsection