# BS System - Base

Base project struktur utama dan untuk APPS-XXX, MAIN-XXX, sudah diset dependency ke Core
## Development Environtment Setup
Untuk proses development yang seragam, pastikan struktur development di local komputer seperti berikut :
> `.\BS-System\` Namespace utama\
`.\BS-System\Core` https://gitlab.com/BS-System/Core \
`.\BS-System\MAIN-Account`https://gitlab.com/BS-System/MAIN-Account \
`.\BS-System\OTHER-REPO`https://gitlab.com/BS-System/OTHER-REPO 

Untuk mengganti lokasi local package dikomputer bisa edit config `DEV_PACKAGE_PATH` di file `.inv` , isi dengan path package ditempatkan (*relative ke project utama*).
 1. **Create Project** 
 Ada 2 cara untuk setup awal base project, yaitu dengan cara composer create-project dan git clone.
	 1. *composer create-project **[BELUM BISA]***\
*Untuk sementara gunakan cara ke dua (git clone).*\
*Eksekusi perintah dibawah ini :*\
 `composer create-project BS-System\Base <PROJECT PATH> --repository-url=httpS://devpm.billionairestore.co.id/repo.json --stability=dev`
	 2. git clone\
Pertama clone dahulu base project (https://gitlab.com/BS-System/Base) dan core (https://gitlab.com/BS-System/Core) sesuai struktur development diatas.
