@extends('layouts-ecourse.app')

@section('layout-content')
@include('layouts-ecourse.includes.layout-navbar-member')
<div class="container-fluid my-5">
    <div class="row">
        <div class="col-lg-3">
            @include('layouts-ecourse.includes.layout-sidenav-member')
        </div>
        <div class="col-lg-9">
            @yield('content')
        </div>
    </div>
</div>
@include('layouts-ecourse.includes.layout-footer')
@endsection
