<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecourse', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tittle')->unique();
            $table->text('description');
            $table->text('thumbnail');
            $table->text('telegram')->nullable();
            $table->text('facebook')->nullable();
            $table->string('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecourse');
    }
}
