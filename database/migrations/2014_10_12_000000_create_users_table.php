<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('phone', 20);
            $table->timestamp('phone_verified_at')->nullable();
            $table->string('password');
            $table->string('auth_password')->nullable();
            $table->boolean('is_admin')->default(0)->nullable();
            $table->string('reff_code')->nullable();
            $table->text('note')->nullable();
            $table->string('role')->nullable();
            $table->boolean('is_active')->default(0);
            $table->tinyInteger('status');
            $table->text('banned_note')->nullable();
            $table->timestamp('bannet_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
